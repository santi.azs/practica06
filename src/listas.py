from src.nodos import Nodo


# Definicion de la clase 'ListaLigada':
class ListaLigada:
    def __init__(self):
        self.cabeza = None

    # Agrega un elemento al final de la lista
    def agregar_elemento(self, valor):
        # Aqui inicia tu codigo
        return

    def buscar_elemento(self, valor):
        # Aqui inicia tu codigo
        return

    def elimina_cabeza(self):
        # Aqui inicia tu codigo
        return

    def elimina_rabo(self):
        # Aqui inicia tu codigo
        return

    def tamagno(self):
        # Aqui inicia tu codigo
        contador = 0
        return contador

    def copia(self):
        nueva_lista = ListaLigada()
        actual = self.cabeza
        while actual:
            nueva_lista.agregar_elemento(actual.valor)
            actual = actual.siguiente
        return nueva_lista

    def __str__(self):
        valores = []
        actual = self.cabeza
        while actual:
            valores.append(str(actual.valor))
            actual = actual.siguiente
        return " -> ".join(valores)
