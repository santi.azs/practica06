from src.listas import ListaLigada

from src.nodos import Nodo

def main():
    # Crear una instancia de ListaLigada
    mi_lista = ListaLigada()

    # Agregar elementos a la lista
    mi_lista.agregar_elemento(1)
    mi_lista.agregar_elemento(2)
    mi_lista.agregar_elemento(3)
    mi_lista.agregar_elemento(4)
# Definicion de la clase 'ListaLigada':
class ListaLigada:
    def _init_(self):
        self.cabeza = None
        #Dado que no hay elementos en la lista
    
    
    # Agrega un elemento al final de la lista
    def agregar_elemento(self, valor):
        if self.cabeza==None:
            self.cabeza = Nodo(valor)
        else:
            actual = self.cabeza
            while actual.siguiente!= None:
                actual = actual.siguiente
            actual.siguiente = Nodo(valor)
        return

    # Imprimir la lista original
    print("Lista original:", mi_lista)
    def buscar_elemento(self, valor):
        if self.cabeza != None:
            actual = self.cabeza
            while actual.siguiente != None:
                if actual.valor == valor:
                    return True
                else:
                    actual = actual.siguiente
        return False

    # Eliminar la cabeza
    mi_lista.elimina_cabeza()
    print("Después de eliminar la cabeza:", mi_lista)
    def elimina_cabeza(self):
        if self.cabeza != None:
            self.cabeza = self.cabeza.siguiente
        return

    # Eliminar el rabo
    mi_lista.elimina_rabo()
    print("Después de eliminar el rabo:", mi_lista)
    def elimina_rabo(self):
        actual = self.cabeza
        if self.cabeza == None:
            return
        elif self.cabeza.siguiente == None:
            self.cabeza = None
        elif actual != None:
            while actual.siguiente.siguiente != None:
                actual = actual.siguiente
            actual.siguiente = None
        return

    # Agregar más elementos
    mi_lista.agregar_elemento(5)
    mi_lista.agregar_elemento(6)
    def tamagno(self):
        contador = 0
        actual = self.cabeza
        while actual != None:
            contador = contador + 1
            actual = actual.siguiente
        return contador

    # Imprimir la lista final
    print("Lista final:", mi_lista)
    def copia(self):
        nueva_lista = ListaLigada()
        actual = self.cabeza
        while actual:
            nueva_lista.agregar_elemento(actual.valor)
            actual = actual.siguiente
        return nueva_lista


if __name__ == "__main__":
    main()
    def _str_(self):
        valores = []
        actual = self.cabeza
        while actual:
            valores.append(str(actual.valor))
            actual = actual.siguiente
        return " -> ".join(valores)

